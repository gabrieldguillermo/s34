// EXPRESS SETUP
// 1. Import by using the 'require' directive to get access to the components of express package/depency
const express = require('express')
// 2. Use the express() function and assign in to an app variable  tp create an express app or app server
const app = express()
// 3. Declare a variable for the port of the server
const port = 3000


// Middlewares
// these two .use are essential in express

// Allows your app to read json data
app.use(express.json());

// Allows your app to read data from forms
app.use(express.urlencoded({extended: true}));

// Routes

// Get request route
app.get('/', (request, response) =>{
	// once the route is accessed it will send a string response containing "Hello World"
	response.send('Hello World');
});

app.get('/hello', (request, response) =>{
	response.send('Hello from /hello enpoint!');
});

// Register user route

// mock database
let users= [];

app.post('/register', (request,response)=>{
	if(request.body.username !== " " && request.body.password !== " "){
		 users.push(request.body)
		 console.log(users);
		 response.send(`User ${request.body.username} successfully registered`)
	} else {
		response.send('Please input BOTH username and password')
	}
})


//Put request route
app.put('/change-password' , (request, response) =>{

	let message

	for(let i = 0; i < users.length; i++ ){
		if(request.body.username == users[i].username){
			users[i].password == request.body.password
			message = `User ${request.body.username}'s password has been updated!`
			break
		} else {
			message = 'User does not exist'
		}
	}
	response.send(message);
})



/*===== Activity =====*/
//1
app.get('/home', (request, response) => response.send('Welcome to Home page'));


//2
app.get('/users', (request,response)=> response.send(users));


//3
app.delete('/delete-user', (request,response)=> {

	let message

	for(let i = 0; i < users.length; i++ ){
		if(request.body.username == users[i].username){
			users.splice(i, 1)
			message = `User ${request.body.username} has been deleted!`
			console.log(users);
			break
		} else {
			message = 'User does not exist'
		}
	}

	response.send(message);
});



app.listen(port, () => console.log(`Server is running at port ${port}`));

